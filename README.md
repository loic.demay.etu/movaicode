# MOVAICODE

Submission Movai Code™ de la team `import iut.team.Java.Monster;`

### Mais que fait ce code?

Ce code donne le nom de la meilleure boisson pour produire le pire code (ou vous pouvez juste regarder le nom de notre équipe).

Le movai code est dans `Movai.java`.

Remerciements : [LingoJam](https://lingojam.com/GlitchTextGenerator)

### Run
- Créer le dossier de sortie : `mkdir -p bin`
- Compiler : `javac -d bin -sourcepath src src/Movai.java`
- Run : `java -cp bin Movai`